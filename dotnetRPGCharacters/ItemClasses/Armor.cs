﻿using dotnetRPGCharacters.Enumerators;
using dotnetRPGCharacters.Interfaces;
using dotnetRPGCharacters.Structs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dotnetRPGCharacters.ItemClasses
{
    public class Armor: Item, IArmor
    {
        //Properties
        public ArmorType ArmorType { get; }
        public PrimaryAttributes ArmorAttributes { get; }


        //Methods

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="name">The armors name.</param>
        /// <param name="itemLevel">The required level to equip the armor.</param>
        /// <param name="slot">The armors equipment slot.</param>
        /// <param name="armorType">The armors armor type.</param>
        /// <param name="primaryAttributes">The armors primary attributes.</param>
        public Armor(string name, int itemLevel, itemSlot slot, ArmorType armorType, PrimaryAttributes primaryAttributes) : base(name, itemLevel, slot)
        {
            this.ArmorType = armorType;
            this.ArmorAttributes += primaryAttributes;
        }
    }
}

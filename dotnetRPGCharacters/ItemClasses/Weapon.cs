﻿using dotnetRPGCharacters.Enumerators;
using dotnetRPGCharacters.Interfaces;
using dotnetRPGCharacters.Structs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dotnetRPGCharacters.ItemClasses
{
    public class Weapon: Item, IWeapon
    {
        //Properties
        public WeaponType WeaponType { get; set; }
        public WeaponAttributes WeaponAttributes { get; set; }

        //Methods

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="name">The weapons name.</param>
        /// <param name="itemLevel">The required level to equip the weapon.</param>
        /// <param name="slot">The weapons item slot. </param>
        /// <param name="weaponType">The weapons type. </param>
        /// <param name="weaponAttributes">The weapons attributes. </param>
        public Weapon(string name, int itemLevel, itemSlot slot, WeaponType weaponType, WeaponAttributes weaponAttributes) : base(name, itemLevel, slot)
        {
            this.WeaponType = weaponType;
            this.WeaponAttributes = new WeaponAttributes { damage = weaponAttributes.damage, attackSpeed = weaponAttributes.attackSpeed };
        }


    }

    

    
}

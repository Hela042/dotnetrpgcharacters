﻿using dotnetRPGCharacters.Enumerators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dotnetRPGCharacters.ItemClasses
{
    public abstract class Item
    {
        //Properties
        public string name { get; set; }
        public int itemLevel { get; set; }
        public itemSlot slot { get; set; }

        //Methods

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="name"> The items name.</param>
        /// <param name="itemLevel">The items rquired level to be equipped.</param>
        /// <param name="slot">The items equipment slot.</param>
        public Item(string name, int itemLevel, itemSlot slot)
        {
            this.name = name;
            this.itemLevel = itemLevel;
            this.slot = slot;
        }
    }


}

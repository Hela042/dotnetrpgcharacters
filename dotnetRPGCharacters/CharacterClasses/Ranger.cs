﻿using dotnetRPGCharacters.CustomExceptions;
using dotnetRPGCharacters.Enumerators;
using dotnetRPGCharacters.Interfaces;
using dotnetRPGCharacters.ItemClasses;
using dotnetRPGCharacters.Structs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dotnetRPGCharacters.CharacterClasses
{
    public class Ranger: Character
    {
        //Methods

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="name">Rangers name.</param>
        public Ranger(string name): base(name)
        {
            this.baseAttributes = new PrimaryAttributes { strength = 1, dexterity = 7, intelligence = 1 };
            this.totalAttributes += this.baseAttributes;
        }

        public override bool canEquip(Item item)
        {

            if (item is IWeapon)
            {
                if (item.itemLevel > this.level)
                {
                    return false;
                }
                IWeapon weapon = (IWeapon)item;
                if (weapon.WeaponType == WeaponType.WEAPON_BOW)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                if (item.itemLevel > this.level)
                {
                    return false;
                }
                IArmor armor = (IArmor)item;
                switch (armor.ArmorType)
                {
                    case ArmorType.ARMOR_MAIL:
                        return true;
                        break;
                    case ArmorType.ARMOR_LEATHER:
                        return true;
                        break;
                    default:
                        return false;
                        break;
                }
            }
        }

        public override double calculateDamage()
        {
            if (this.equipment.ContainsKey(itemSlot.SLOT_WEAPON))
            {
                IWeapon equippedWeapon = (IWeapon)this.equipment[itemSlot.SLOT_WEAPON];
                double weaponDPS = equippedWeapon.WeaponAttributes.attackSpeed * equippedWeapon.WeaponAttributes.damage;
                return weaponDPS * (1 + ((double)calculateTotalAttributes().dexterity / (double)100));
            }
            else
            {
                return 1 * (1 + ((double)calculateTotalAttributes().dexterity / (double)100));
            }
        }

        public override int levelUp()
        {
            this.level++;
            this.totalAttributes += new PrimaryAttributes { strength = 1, dexterity = 5, intelligence = 1 };
            return this.level;
        }
    }
}

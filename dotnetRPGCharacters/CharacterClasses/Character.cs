﻿using dotnetRPGCharacters.Enumerators;
using dotnetRPGCharacters.Interfaces;
using dotnetRPGCharacters.ItemClasses;
using dotnetRPGCharacters.Structs;
using dotnetRPGCharacters.CustomExceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dotnetRPGCharacters.CharacterClasses
{
    public abstract class Character
    {
        //Properties
        public string name { get; set; }
        public int level { get; set; }
        public PrimaryAttributes baseAttributes { get; set; }
        public PrimaryAttributes totalAttributes { get; set; }
        public Dictionary<itemSlot, Item> equipment { get; set; }


        //Methods

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="name">Characters name.</param>
        public Character(string name)
        {
            this.name = name;
            this.level = 1;
            this.equipment = new Dictionary<itemSlot, Item>();
        }
        /// <summary>
        /// Generates the characters character sheet.
        /// </summary>
        /// <returns>Returns a StringBuilder.</returns>
        public StringBuilder getCharacterSheet()
        {
            StringBuilder characterSheet = new StringBuilder();
            PrimaryAttributes totalAttr = calculateTotalAttributes();

            characterSheet.Append($"Name: {this.name}\n");
            characterSheet.Append($"Level: {this.level}\n");
            characterSheet.Append($"Strength: {totalAttr.strength}\n");
            characterSheet.Append($"Dexterity: {totalAttr.dexterity}\n");
            characterSheet.Append($"Intelligence: {totalAttr.intelligence}\n");
            characterSheet.Append($"Damage: {this.calculateDamage()}\n");
            return characterSheet;
        }
        /// <summary>
        /// Levels the character up by 1 level and increases the characters stats by the appropriate amount.
        /// </summary>
        /// <returns> returns the characters new level. </returns>

        public abstract int levelUp();
        /// <summary>
        /// Calculates the characters damage based on the character weapon dps and primary attributes.
        /// If no weapon is equipped the characters weapon dps is 1.
        /// </summary>
        /// <returns> Returns the characters damage. </returns>
        public abstract double calculateDamage();
        /// <summary>
        /// Determines if the passed item can be equipped or not.
        /// </summary>
        /// <param name="item"> Item to equip. </param>
        /// <returns>Returns true if the weapon can be equipped or returns false if the item cannot be equipped.</returns>
        public abstract bool canEquip(Item item);
        /// <summary>
        /// Attempts to equip the passed in item. Will add the item into the characters equipment property if
        /// the item slot has not been filled. If the item slot already is filled the new item will replace the old one.
        /// </summary>
        /// <param name="item"> Item to equip </param>
        /// <exception cref="InvalidArmorException">
        /// Thrown if the armor cannot be equipped.</exception>
        /// <exception cref="InvalidWeaponException">
        /// Thrown if the weapon cannot be equipped.</exception>
        /// <returns> Returns a string signaling that the item was equipped or throws an appropriate exception if the item cannot be equipped. </returns>
        public string equipItem(Item item)
        {
            if (canEquip(item))
            {
                if (this.equipment.ContainsKey(item.slot))
                {
                    this.equipment[item.slot] = item;
                }
                else
                {
                    this.equipment.Add(item.slot, item);
                }
                if (item.slot == itemSlot.SLOT_WEAPON)
                {
                    return "New weapon equipped!";
                }
                else
                {
                    return "New armor equipped!";
                }
            }
            else
            {
                if (item is IWeapon)
                {
                    throw new InvalidWeaponException();
                }
                else
                {
                    throw new InvalidArmorException();
                }
            }
        }
        /// <summary>
        /// Calculates the characters total attributes.
        /// </summary>
        /// <returns>Returns the calculated total attributes.</returns>
        public PrimaryAttributes calculateTotalAttributes()
        {
            PrimaryAttributes totalAttributes = new PrimaryAttributes();
            totalAttributes += this.totalAttributes;
            foreach (var item in this.equipment)
            {
                if (item.Value is IArmor)
                {
                    IArmor armor = (IArmor)item.Value;
                    totalAttributes += armor.ArmorAttributes;
                }
            }
            return totalAttributes;
        }


    }
}

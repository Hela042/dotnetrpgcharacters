﻿using dotnetRPGCharacters.CustomExceptions;
using dotnetRPGCharacters.Enumerators;
using dotnetRPGCharacters.Interfaces;
using dotnetRPGCharacters.ItemClasses;
using dotnetRPGCharacters.Structs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dotnetRPGCharacters.CharacterClasses
{
    public class Mage: Character
    {
        //Methods

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="name">Mages name.</param>
        public Mage(string name): base(name)
        {
            this.baseAttributes = new PrimaryAttributes { strength = 1, dexterity = 1, intelligence = 8 };
            this.totalAttributes += this.baseAttributes;
        }

        public override int levelUp()
        {
            this.level++;
            this.totalAttributes += new PrimaryAttributes { strength = 1, dexterity = 1, intelligence = 5 };
            return this.level;
        }

        public override bool canEquip(Item item)
        {
            

            if (item is IWeapon)
            {
                if (item.itemLevel > this.level)
                {
                    return false;
                }
                IWeapon weapon = (IWeapon)item;
                switch (weapon.WeaponType)
                {
                    case WeaponType.WEAPON_STAFF:
                        return true;
                        break;
                    case WeaponType.WEAPON_WAND:
                        return true;
                        break;
                    default:
                        return false;
                        break;
                }
            }
            else
            {
                if (item.itemLevel > this.level)
                {
                    return false;
                }
                IArmor armor = (IArmor)item;
                if (armor.ArmorType == ArmorType.ARMOR_CLOTH)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        public override double calculateDamage()
        {
            if (this.equipment.ContainsKey(itemSlot.SLOT_WEAPON))
            {
                IWeapon equippedWeapon = (IWeapon)this.equipment[itemSlot.SLOT_WEAPON];
                double weaponDPS = equippedWeapon.WeaponAttributes.attackSpeed * equippedWeapon.WeaponAttributes.damage;
                return weaponDPS * (1 + ((double)calculateTotalAttributes().intelligence / (double)100));
            }
            else
            {
                return 1 * (1 + ((double)calculateTotalAttributes().intelligence / (double)100));
            }
        }
    }
}

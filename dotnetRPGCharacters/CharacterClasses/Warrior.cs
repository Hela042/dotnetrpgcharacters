﻿using dotnetRPGCharacters.CustomExceptions;
using dotnetRPGCharacters.Enumerators;
using dotnetRPGCharacters.Interfaces;
using dotnetRPGCharacters.ItemClasses;
using dotnetRPGCharacters.Structs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dotnetRPGCharacters.CharacterClasses
{
    public class Warrior: Character
    {
        //Methods

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="name">Warriors name.</param>
        public Warrior(string name) : base(name)
        {
            this.baseAttributes = new PrimaryAttributes { strength = 5, dexterity = 2, intelligence = 1 };
            this.totalAttributes += this.baseAttributes;
        }

        public override int levelUp()
        {
            this.level++;
            this.totalAttributes += new PrimaryAttributes { strength = 3, dexterity = 2, intelligence = 1 };
            return this.level;
        }

        public override bool canEquip(Item item)
        {

            if (item is IWeapon)
            {
                if (item.itemLevel > this.level)
                {
                    return false;
                }
                IWeapon weapon = (IWeapon)item;
                switch (weapon.WeaponType)
                {
                    case WeaponType.WEAPON_AXE:
                        return true;
                        break;
                    case WeaponType.WEAPON_HAMMER:
                        return true;
                        break;
                    case WeaponType.WEAPON_SWORD:
                        return true;
                        break;
                    default:
                        return false;
                        break;
                }
            }
            else
            {
                if (item.itemLevel > this.level)
                {
                    return false;
                }
                IArmor armor = (IArmor)item;
                switch (armor.ArmorType)
                {
                    case ArmorType.ARMOR_MAIL:
                        return true;
                        break;
                    case ArmorType.ARMOR_PLATE:
                        return true;
                        break;
                    default:
                        return false;
                        break;
                }
            }
        }
        public override double calculateDamage()
        {
            if (this.equipment.ContainsKey(itemSlot.SLOT_WEAPON))
            {
                IWeapon equippedWeapon = (IWeapon)this.equipment[itemSlot.SLOT_WEAPON];
                double weaponDPS = equippedWeapon.WeaponAttributes.attackSpeed * equippedWeapon.WeaponAttributes.damage;
                return weaponDPS * (1 + ((double)calculateTotalAttributes().strength / (double)100));
            }
            else
            {
                return 1 * (1 + ((double)calculateTotalAttributes().strength / (double)100));
            }
        }
    }
}

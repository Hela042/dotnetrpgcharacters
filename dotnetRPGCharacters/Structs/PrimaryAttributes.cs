﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dotnetRPGCharacters.Structs
{
    public struct PrimaryAttributes
    {
        public int strength;
        public int intelligence;
        public int dexterity;
        /// <summary>
        /// Addition operator override to properly add two primaryAttributes structs.
        /// </summary>
        /// <param name="leftHand"></param>
        /// <param name="rightHand"></param>
        /// <returns> The sum of the two added PrimaryAttributes.</returns>
        public static PrimaryAttributes operator +(PrimaryAttributes leftHand, PrimaryAttributes rightHand)
            => new PrimaryAttributes { 
                strength = leftHand.strength + rightHand.strength,
                intelligence = leftHand.intelligence + rightHand.intelligence,
                dexterity = leftHand.dexterity + rightHand.dexterity 
            };
    }
}

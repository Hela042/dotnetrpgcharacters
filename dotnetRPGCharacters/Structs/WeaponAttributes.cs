﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dotnetRPGCharacters.Structs
{
    public struct WeaponAttributes
    {
        public int damage;
        public double attackSpeed;
    }
}

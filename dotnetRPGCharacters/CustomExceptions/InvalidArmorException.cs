﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dotnetRPGCharacters.CustomExceptions
{
    public class InvalidArmorException: Exception
    {
        public InvalidArmorException()
        {
        }

        public InvalidArmorException(string message): base(message)
        {
        }

        public override string Message => "Cannot equip that armor";
    }
}

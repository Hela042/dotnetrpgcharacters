﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dotnetRPGCharacters.Enumerators
{
    public enum itemSlot
    {
        SLOT_WEAPON,
        SLOT_HEAD,
        SLOT_BODY,
        SLOT_LEGS
    }
}

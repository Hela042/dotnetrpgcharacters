﻿using dotnetRPGCharacters.Enumerators;
using dotnetRPGCharacters.ItemClasses;
using dotnetRPGCharacters.Structs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dotnetRPGCharacters.Interfaces
{
    public interface IWeapon
    {
        WeaponType WeaponType { get; set; }
        WeaponAttributes WeaponAttributes { get; set; }
    }
}

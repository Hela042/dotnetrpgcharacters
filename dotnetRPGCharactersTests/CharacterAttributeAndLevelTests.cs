using dotnetRPGCharacters.CharacterClasses;
using dotnetRPGCharacters.Structs;
using System;
using Xunit;

namespace dotnetRPGCharactersTests
{
    public class CharacterAttributeAndLevelTests
    {
        #region base character tests
        [Fact]
        public void CharacterConstructor_CharacterCreation_CharcterLevelIs1()
        {
            //Arrange
            Mage testMage = new Mage("test");
            int expected = 1;
            //Act
            int actual = testMage.level;
            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void CharacterLevelUp_LevelUpCharacter_CharacterLevel2()
        {
            //Arrange
            Mage testMage = new Mage("test");
            int expected = 2;
            //Act
            int actual = testMage.levelUp();
            //Assert
            Assert.Equal(expected, actual);
        }
        #endregion

        #region class base attributes tests

        [Fact]
        public void MageConstructor_MageCreation_MageBaseAttributesCorrect()
        {
            //Arrange
            Mage testMage = new Mage("test");
            PrimaryAttributes expected = new PrimaryAttributes() { strength=1, dexterity=1, intelligence=8};
            //Act
            PrimaryAttributes actual = testMage.baseAttributes;
            //Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void WarriorConstructor_WarriorCreation_WarriorBaseAttributesCorrect()
        {
            //Arrange
            Warrior testWarrior = new Warrior("test");
            PrimaryAttributes expected = new PrimaryAttributes() { strength = 5, dexterity = 2, intelligence = 1 };
            //Act
            PrimaryAttributes actual = testWarrior.baseAttributes;
            //Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void RangerConstructor_RangerCreation_RangerBaseAttributesCorrect()
        {
            //Arrange
            Ranger testRanger = new Ranger("test");
            PrimaryAttributes expected = new PrimaryAttributes() { strength = 1, dexterity = 7, intelligence = 1 };
            //Act
            PrimaryAttributes actual = testRanger.baseAttributes;
            //Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void RogueConstructor_RogueCreation_RogueBaseAttributesCorrect()
        {
            //Arrange
            Rogue testRogue = new Rogue("test");
            PrimaryAttributes expected = new PrimaryAttributes() { strength = 2, dexterity = 6, intelligence = 1 };
            //Act
            PrimaryAttributes actual = testRogue.baseAttributes;
            //Assert
            Assert.Equal(expected, actual);
        }

        #endregion

        #region class attributes after level up tests

        [Fact]
        public void MageLevelUp_MageLevelUp_MageLevelUpAttributesGrowthCorrect()
        {
            //Arrange
            Mage testMage = new Mage("test");
            PrimaryAttributes expected = new PrimaryAttributes { strength = 2, dexterity = 2, intelligence = 13 };
            //Act
            testMage.levelUp();
            PrimaryAttributes actual = testMage.totalAttributes;
            //Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void WarriorLevelUp_WarriorLevelUp_WarriorLevelUpAttributesGrowthCorrect()
        {
            //Arrange
            Warrior testWarrior = new Warrior("test");
            PrimaryAttributes expected = new PrimaryAttributes { strength = 8, dexterity = 4, intelligence = 2 };
            //Act
            testWarrior.levelUp();
            PrimaryAttributes actual = testWarrior.totalAttributes;
            //Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void RangerLevelUp_RangerLevelUp_RangerLevelUpAttributesGrowthCorrect()
        {
            //Arrange
            Ranger testRanger = new Ranger("test");
            PrimaryAttributes expected = new PrimaryAttributes { strength = 2, dexterity = 12, intelligence = 2 };
            //Act
            testRanger.levelUp();
            PrimaryAttributes actual = testRanger.totalAttributes;
            //Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void RogueLevelUp_RogueLevelUp_RogueLevelUpAttributesGrowthCorrect()
        {
            //Arrange
            Rogue testRogue = new Rogue("test");
            PrimaryAttributes expected = new PrimaryAttributes { strength = 3, dexterity = 10, intelligence = 2 };
            //Act
            testRogue.levelUp();
            PrimaryAttributes actual = testRogue.totalAttributes;
            //Assert
            Assert.Equal(expected, actual);
        }
        #endregion
    }
}

﻿using dotnetRPGCharacters.CharacterClasses;
using dotnetRPGCharacters.CustomExceptions;
using dotnetRPGCharacters.Enumerators;
using dotnetRPGCharacters.ItemClasses;
using dotnetRPGCharacters.Structs;
using System;
using Xunit;

namespace dotnetRPGCharactersTests
{
    public class ItemsAndEquipmentTests
    {
        #region equip invalid item tests
        [Fact]
        public void CharacterEquipItem_EquipHighItemLevelWeapon_ThrowInvalidWeaponException()
        {
            //Arrange
            Warrior testWarrior = new Warrior("test");
            Weapon testAxe = new Weapon("Common Axe", 2, itemSlot.SLOT_WEAPON, WeaponType.WEAPON_AXE, new WeaponAttributes { damage = 7, attackSpeed = 1.1 });
            //Act & Assert
            Assert.Throws<InvalidWeaponException>(() => testWarrior.equipItem(testAxe));
        }
        [Fact]
        public void CharacterEquipItem_EquipHighItemLevelArmor_ThrowInvalidArmorException()
        {
            //Arrange
            Warrior testWarrior = new Warrior("test");
            Armor testPlate = new Armor("Common plate body armor", 2, itemSlot.SLOT_BODY, ArmorType.ARMOR_PLATE, new PrimaryAttributes { strength = 1 });
            //Act & Assert
            Assert.Throws<InvalidArmorException>(() => testWarrior.equipItem(testPlate));
        }
        [Fact]
        public void CharacterEquipItem_EquipWrongWeaponType_ThrowInvalidWeaponException()
        {
            //Arrange
            Warrior testWarrior = new Warrior("test");
            Weapon testBow = new Weapon("Common Bow", 1, itemSlot.SLOT_WEAPON, WeaponType.WEAPON_BOW, new WeaponAttributes() { damage = 12, attackSpeed = 0.8 });
            //Act & Assert
            Assert.Throws<InvalidWeaponException>(() => testWarrior.equipItem(testBow)); 
        }
        [Fact]
        public void CharacterEquipItem_EquipWrongArmorType_ThrowInvalidArmorException()
        {
            //Arrange
            Warrior testWarrior = new Warrior("test");
            Armor testCloth = new Armor("Common cloth head armor", 1, itemSlot.SLOT_HEAD, ArmorType.ARMOR_CLOTH, new PrimaryAttributes { strength = 5 });
            //Act & Assert
            Assert.Throws<InvalidArmorException>(() => testWarrior.equipItem(testCloth));
        }
        #endregion


        #region equip valid item tests
        [Fact]
        public void CharacterEquipItem_EquipValidWeapon_ReturnSuccessMessage()
        {
            //Arrange
            Warrior testWarrior = new Warrior("test");
            Weapon testAxe = new Weapon("Common Axe", 1, itemSlot.SLOT_WEAPON, WeaponType.WEAPON_AXE, new WeaponAttributes { damage = 7, attackSpeed = 1.1 });
            string expected = "New weapon equipped!";
            //Act
            string actual = testWarrior.equipItem(testAxe);
            //Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void CharacterEquipItem_EquipValidArmor_ReturnSuccessMessage()
        {
            //Arrange
            Warrior testWarrior = new Warrior("test");
            Armor testPlate = new Armor("Common plate body armor", 1, itemSlot.SLOT_BODY, ArmorType.ARMOR_PLATE, new PrimaryAttributes { strength = 1 });
            string expected = "New armor equipped!";
            //Act
            string actual = testWarrior.equipItem(testPlate);
            //Assert
            Assert.Equal(expected, actual);
        }
        #endregion


        #region character damage tests
        [Fact]
        public void CharacterCalculateDamage_DamageNoWeapon_ReturnCorrectDamage()
        {
            //Arrange
            Warrior testWarrior = new Warrior("test");
            double expected = 1 * (1 + ((double)5 / (double)100));
            //Act
            double actual = testWarrior.calculateDamage();
            //Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void CharacterCalculateDamage_DamageWithWeapon_ReturnCorrectDamage()
        {
            //Arrange
            Warrior testWarrior = new Warrior("test");
            Weapon testAxe = new Weapon("Common Axe", 1, itemSlot.SLOT_WEAPON, WeaponType.WEAPON_AXE, new WeaponAttributes { damage = 7, attackSpeed = 1.1 });
            testWarrior.equipItem(testAxe);
            double expected = (7 * 1.1) * (1 + ((double)5 / (double)100));
            //Act
            double actual = testWarrior.calculateDamage();
            //Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void CharacterCalculateDamage_DamageWithWeaponAndArmor_ReturnCorrectDamage()
        {
            //Arrange
            Warrior testWarrior = new Warrior("test");
            Weapon testAxe = new Weapon("Common Axe", 1, itemSlot.SLOT_WEAPON, WeaponType.WEAPON_AXE, new WeaponAttributes { damage = 7, attackSpeed = 1.1 });
            testWarrior.equipItem(testAxe);
            Armor testPlate = new Armor("Common plate body armor", 1, itemSlot.SLOT_BODY, ArmorType.ARMOR_PLATE, new PrimaryAttributes { strength = 1 });
            testWarrior.equipItem(testPlate);
            double expected = (7 * 1.1) * (1 + ((double)6 / (double)100));
            //Act
            double actual = testWarrior.calculateDamage();
            //Assert
            Assert.Equal(expected, actual);
        }
        #endregion
    }
}

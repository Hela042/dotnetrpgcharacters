# Assignment Build a console application in C#

Assignment for Noroff Accelerate winter 2022. Task is to build a console application in C# to create RPG Characters.

## Description

The task is to create a class library which gives the user the ability to create characters of four different RPG classes. Aswell a to create weapons and armor to be equipped by the characters.


## Getting Started

### Dependencies

* .Net Framework
* Visual Studio 2019/22 OR Visual Studio Code

### Executing program
* clone repository / download
* Open solution in Visual Studio
* Build and run

## Author

Lukas Mårtensson [@Hela042]
